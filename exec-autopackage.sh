#!/usr/bin/env bash

# 必要に応じてを書き換えてください
LOCATION="asia-northeast1"
JOB_NAME="custom job"
MACHINE_TYPE="n1-standard-4" # 仮想マシンのスペック
REPLICA_COUNT=1   # 分散数
EXECUTOR_IMAGE_URI="asia-docker.pkg.dev/vertex-ai/training/tf-gpu.2-14.py310:latest" # Google提供の事前構成済みのコンテナイメージのURI
WORKING_DIRECTORY="./"
SCRIPT_PATH="./main.py"
ACCELERATOR_TYPE="NVIDIA_TESLA_T4" # GPUの種類
ACCELERATOR_COUNT=1 # GPUの数

gcloud ai custom-jobs create \
--region="${LOCATION}" \
--display-name="${JOB_NAME}" \
--worker-pool-spec=machine-type="${MACHINE_TYPE}",replica-count="${REPLICA_COUNT}",executor-image-uri="${EXECUTOR_IMAGE_URI}",local-package-path="${WORKING_DIRECTORY}",script="${SCRIPT_PATH}",accelerator-type="${ACCELERATOR_TYPE}",accelerator-count="${ACCELERATOR_COUNT}"
