import tensorflow as tf

# GPU デバイスのリストを取得
gpus = tf.config.list_physical_devices('GPU')

if gpus:
    print("Available GPU devices:")
    for gpu in gpus:
        print(f"- {gpu.name} (type: {gpu.device_type})")

    try:
        # GPU を TensorFlow で利用可能にする
        tf.config.experimental.set_memory_growth(gpus[0], True)
        logical_gpus = tf.config.list_logical_devices('GPU')
        print(f"\nLogical GPU devices: {len(logical_gpus)}")
    except RuntimeError as e:
        print(e)
else:
    print("No GPU devices available.")


print("-----------------------------------------------")
print("Execution test")