# VertexAI　CustomJob実行サンプル

このサンプルでは、Google Cloud の Vertex AI で Custom Job を実行する方法を分かりやすく解説します。 具体的には、事前にコンテナをビルドし、ジョブ実行時に指定する方法をステップバイステップで説明します。

## はじめに

Vertex AI Custom Job を実行するには、事前にジョブ実行用のコンテナイメージを作成し、Artifact Registry に保存しておく必要があります。この方法は、TensorFlow や PyTorch など、大規模なライブラリを使用する場合や、同じコンテナイメージを複数のジョブで使い回す場合に特に有効です。

事前に`Artifact Registry`にジョブ実行用のイメージレジストリを作成します。

## ステップ 1: コンテナイメージのビルドとプッシュ

### 1. Dockerfile の準備

- ベースイメージを指定します。TensorFlow や GPU を使用する場合、適切なイメージを選択してください。
- 必要なライブラリを requirements.txt に記述します。
- ジョブ実行のエントリポイントとなる Python スクリプト (main.py など) を作成します。
- Dockerfile 内で、ENTRYPOINT ["python", "main.py"] のようにエントリポイントを指定します。ファイル名は適宜変更可能です。

### 2. build-push.sh の設定

- REPO_NAME と IMAGE_NAME を、環境に合わせて変更します。
- IMAGE_URI のリージョン (asia-northeast1-docker.pkg.dev の部分) を確認し、必要であれば変更します。

### 3. コンテナイメージのビルドとプッシュ

- 上記の設定が完了したら、build-push.sh を実行します。
- ビルドが完了すると、コンソールにイメージの URL が表示されます。この URL は、後のステップで必要になるので、メモしておいてください。

## ステップ 2: Custom Job の実行

### 1. exec.sh の設定

- GPU を使用する場合は、exec.sh 内の ACCELERATOR_TYPE と ACCELERATOR_COUNT を必要に応じて変更します。
- GPU を使用しない場合は、--worker-pool-spec オプションから accelerator-type と accelerator-count を削除します。

exec.shの該当箇所
```
ACCELERATOR_TYPE="NVIDIA_TESLA_T4" # GPUの種類
ACCELERATOR_COUNT=1 # GPUの数
```
### 2.Custom Job の実行

- ./exec.sh CONTAINER_IMAGE_URI のように、コンテナイメージの URL を引数に指定して exec.sh を実行します。

### 3. ジョブの状況確認

ジョブの実行状況は、コンソールに表示されるコマンドで確認できます。
また、ログもコンソールに出力されます。

```
Using endpoint [https://asia-northeast1-aiplatform.googleapis.com/]
CustomJob [projects/XXXXXXX/locations/asia-northeast1/customJobs/XXXXXXX] is submitted successfully.

Your job is still active. You may view the status of your job with the command

  $ gcloud ai custom-jobs describe projects/XXXXXXX/locations/asia-northeast1/customJobs/XXXXXXX

or continue streaming the logs with the command

  $ gcloud ai custom-jobs stream-logs projects/XXXXXXX/locations/asia-northeast1/customJobs/XXXXXXX
```

## 補足: Autopackaging について

exec-autopackage.sh を使用すると、コンテナのビルドとプッシュ、Custom Job の実行をまとめて行うことができます。ただし、実行のたびにコンテナがビルドされるため、大規模なイメージやパッケージを使用する場合は、実行に時間がかかることに注意してください。
