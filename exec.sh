#!/usr/bin/env bash

# 必要に応じてを書き換えてください
LOCATION="asia-northeast1"
JOB_NAME="custom job"
MACHINE_TYPE="n1-standard-4" # 仮想マシンのスペック
REPLICA_COUNT=1   # 分散数
ACCELERATOR_TYPE="NVIDIA_TESLA_T4" # GPUの種類
ACCELERATOR_COUNT=1 # GPUの数

if [ $# -ne 1 ]; then
  echo "Usage: $0 CONTAINER_IMAGE_URI"
  echo "CONTAINER_IMAGE_URI is required"
  echo ""
  echo "e.g. asia-northeast1-docker.pkg.dev/PROJECT_ID/REPO_NAME/IMAGE_NAME:IMAGE_TAG"
  echo ""
  exit 1
fi

CONTAINER_IMAGE_URI=$1

gcloud ai custom-jobs create \
--region="${LOCATION}" \
--display-name="${JOB_NAME}" \
--worker-pool-spec=machine-type="${MACHINE_TYPE}",replica-count="${REPLICA_COUNT}",container-image-uri="${CONTAINER_IMAGE_URI}",accelerator-type="${ACCELERATOR_TYPE}",accelerator-count="${ACCELERATOR_COUNT}"
