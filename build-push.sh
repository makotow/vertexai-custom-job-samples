#!/usr/bin/env bash

export PROJECT_ID=$(gcloud config list project --format "value(core.project)")

## 環境にあわせて書き換えてください
export REPO_NAME="vertexai-custom-job"
export IMAGE_NAME="vertexai-custom-job-container"
export IMAGE_TAG=$(date +%Y%m%d%H%M)
export IMAGE_URI=asia-northeast1-docker.pkg.dev/${PROJECT_ID}/${REPO_NAME}/${IMAGE_NAME}:${IMAGE_TAG}
## ここまで

docker build -f Dockerfile -t ${IMAGE_URI} ./
docker push ${IMAGE_URI}

echo "----------"

echo "IMAGE_URI:"
echo ${IMAGE_URI}
