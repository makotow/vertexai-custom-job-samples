# Specifies base image and tag
FROM asia-docker.pkg.dev/vertex-ai/training/tf-gpu.2-14.py310:latest
RUN mkdir /app
WORKDIR /app

# Installs additional packages
COPY ./requirements.txt /app/
RUN pip install -r requirements.txt

# Copies the trainer code to the docker image.
COPY main.py /app/main.py

# Sets up the entry point to invoke the trainer.
ENTRYPOINT ["python", "main.py"]